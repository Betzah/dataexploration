---
title: "R Notebook"
output:
  html_document: default
  html_notebook: default
---

# Introductie tot Data Wrangling {#wrangle-intro}

Tot nu toe heb je te maken gehad met georganiseerde en opgeschoonde datasets. In dit hoofdstuk leer je meer over data cleaning/wrangling, zodat je ook met ruwe en vervuilde data bruikbaar kunt maken voor visualisatie en modelleringsdoeleinden.

Er komen drie onderdelen kijken bij data wrangling, namelijk:

![](http://r4ds.had.co.nz/diagrams/data-science-wrangle.png)



Wij zullen vanaf hier de volgende onderwerpen behandelen:

*   __tibbles__, hier leer je een variant van de base-R data frame kennen die we zullen toelichten

*   __tidy data__, hier leer je wat tidy data is, de principes achter tidy data, reshape functies, en de splits en combineer functies.

*   __data import__, hier leer je hoe je data kunt inlezen vanuit je disk naar R. 



Data wrangling omvat ook data transformaties, waar je nu inmiddels al kennis mee hebt gemaakt. We zullen hier nu verder op ingaan echter ook bewerkingen en transformaties behandelen van R's data-typen: 
    
*   __Strings__, hier zul je kennis maken met string manipulatie functies

*   __Dates and times__ we zullen de meest belangrijke datum en tijd manipulatie functies met je delen. 

*   __Factors__ worden door R gebruikt om categorische data op te slaan. 

*   __Special Values__ we zullen byzondere waarden beschouwen zoals "NA", "NULL", "Inf", en "NaN"





## Tibbles

In iedere programeer taal heb je data typen en data structuren. In de vorige module hebben we de verschillende data typen die we in R tegen kunnen komen opgesomd, en behandeld. We hebben echter nog niet expliciet de verschillende data structuren die er bestaan binnen R bestaan benoemd of behandeld. We zullen hier kort op ingaan, omdat het fundamenteel belangrijk is om goed met R te kunnen werken.

### vectoren

De meest basic data structuur in R is de vector. Een R vector is een opvolging van waarden van dezelfde data type. Alle operaties in R werken op vectoren (bijvb. eenvoudige elementsgewijze rekenkunde). Omdat vectoren eisen dat alle elementen van het zelfde data type zijn, zijn er evenveel smaken van de vectoren als er data typen zijn:

```{r results = 'asis'}
library(tidyverse)
datatypen = tibble(`Data Type`=c("numeric","integer","factor", "ordered","character","raw"), 
                        `Beschrijvig` =c("Dit zijn numerieke data (approximaties van reeële getallen)","Dit zijn gehele getallen (integer data)","Dit zijn categorische data (eenvoudige classificaties: geslacht)", "Dit zijn geordende categorische data(onderwijs niveau)","Dit is karakter data (strings)","Dit is Binaire data"))
knitr::kable(datatypen, caption = "Beschrijving van data typen")

```

Alle basis operaties in R werken elementsgewijs op vectoren waar de kortste vector, indien nodig, gerecycled wordt.

```{r}
# vectoren zijn een opeenvolging van waarden van hetzelfde data type
c(1, 2, "drie")
```
```{r}
# kortere variabele worden gerecycled
(1:3) * 2
```
```{r}
(1:4) * c(1, 2)
```
```{r}
# waarschuwing! (waarom?)
(1:4) * (1:3)
```

Iedere element van een vector kan ook een naam toegekend krijgen:

```{r}
AutoKleur = c(bmw = "rood", vw = "blauw", seat = "groen")
```

Elementen van een vector kunnen worden geselecteerd of vervangen door de vierkante haken te gebruiken [ ]. De vierkante haken accepteren een vector van namen, index nummers, of een logical. In geval van een logical, wordt de index gerecycled als deze korter is dan de geindexeerde vector. In geval van een numerieke index, wordt er met negatieve getallen vector-elementen gedropt, en anders worden ze geselecteerd. Een combinatie van negatieve en positieve indices is niet toegestaan. Je kunt een naam of een index nummer wel herhalen, maar dat resulteert ook tot meerdere elementen van de zelfde waarde.

```{r}
names(AutoKleur)[AutoKleur == "rood"]
```
```{r}
x <- c(4, 7, 6, 5, 2, 8)
```
```{r}
I <- x < 6
```
```{r}
J <- x > 7
```
```{r}
I
J
x[I | J]
```
```{r}
x[c(TRUE, FALSE)]
```
```{r}
x[c(-1, -2)]
```
Het vervangen van waarden in vectoren geschied op een soortgelijke wijze. Hoe verklaar je het volgende resultaat?

```{r}
x <- 1:10
```
```{r}
x[c(TRUE, FALSE)] <- 1
x
```


### Lijsten

Een lijst is een generalisatie van een vector, in dat het objecten kan bevatten van verschillende data typen, inclusief andere lijsten.Er zijn twee manieren om een lijst te indexeren. Een enkelvoudige haak operator ([ ]) retourneert altijd een sub-lijst van de geindexeerde lijst. Dat wl zeggen, het resultaat is weer een lijst. De dubbele haken operator ([[  ]]) kan alleen een enige item retourneren, en dat is de object die op dat index in de lijst is opgeslagen. Naast de haak operator, kan ook de dollar operator gebruikt worden om een item op te vragen. Probeer het onderstaande eens uit:


```{r}
L <- list(x = c(1:5), 
          y = c("a", "b", "c"), 
          z = AutoKleur)
L
```
```{r}
L[[2]]
```
```{r}
L$y
```
```{r}
L[c(1, 3)]

```
```{r}
L[c("x", "z")]
```
```{r}
d<-L[["z"]]
d["bmw"] 
```

Je kunt de `class()` functie gebruiken om te bepalen wat voor data type het resultaat retourneert:

```{r}
L[["z"]] 
```


### Data Frames


Data frames zijn niet veel meer dan een lijst van vectoren, die mogelijk verschillende data typen bevatten, maar waarvan iedere vector (nu een kolom) dezelfde lengte heeft. Aangezien dataframes een soort lijzt zijn, zullen enkelvoudige indices sub-dataframes retourneren, dwz, dataframes met minder kolommen.

Op eenzelfde wijze, retourneert de dollar operator een vector, en niet een sub-dataframe. Rijen kunnen geindexeerd worden door gebruik te maken van de dubbele index haak operator ([ x,y ]). De eerste index verwijst naar de rij(en), en de tweede index verwijst naar de kolom(men). Indien een van de indices is weggelaten, wordt er geen selectie gemaakt, en dus alle rijen (of alle kollommen) geretourneerd.

```{r}
d <- data.frame(x = 1:10, 
                y = letters[1:10], 
                z = LETTERS[1:10])
d
```
```{r}
d[1]
```
```{r}
d[, 1]
```
```{r}
d[,c("x", "z")]
```

Wat gebeurt er indien we `drop = FALSE` toevoegen aan de index?

```{r}
d[, "x", drop = FALSE]
```
```{r}
d$x > 3
d[d$x > 3, "y", drop = FALSE]

```
```{r}
d
d[2, ]

```


### Tibbles

Tibbels zijn "moderne" dataframes, die het net iets fijner zijn om mee te werken dan de traditionele dataframes van Base-R. De twee voornaamste verschillen in het gebruik van een tibble tegenover de klassieke dataframe is het printen van de dataframe en het subsetten (ofwel indexeren) ervan.We zullen de verschillen hieronder behandelen.

Laten we een tibble creeëren met gesimuleerde transactie data:

```{r} 
library(tidyverse)
library(lubridate)
library(stringr)
```
```{r}
Betaal_transacties <- tibble( 
  Bedrag = (runif(12,0,200) %>% round(digits = 2)), 
  Betaal_Kenmerk = rep("BA",12), 
  Datum_Boeking = ((seq(0,11) %>% months) + (floor(runif(12,0,6)) %>% days) + 
                     now()) %>% str_sub(1,10) %>% str_replace_all("-","") %>% as.numeric(),
  Tijd_boeking = str_c(floor(runif(12,0,23)),":",floor(runif(12,0,59)),":",floor(runif(12,0,59))),
  RekeningNr_Ontvanger = str_c("NL", floor(runif(12,10,99)),
                               base::sample(c("ABNA","INGB","SNSB"),12,replace=TRUE),
                               runif(12,1000000000,9000000000)), 
  RekeningNr_Zender = rep("NL00INGB0009999999",12))
```
```{r}
Betaal_transacties
```

Een handig voordeel van een tibble is dat het ontworpen is om de tabel presenteerbaar te printen, zodat je makkelijker en sneller een beeld kunt vormen over de data. Een bijkomen voordeel is dat je console niet overwelmd wordt met het printen van enorme data frames. 

Een ander voordeel is subsetten. We hebben eerder gezien dat de dollar operator ($) en de haak operator ([[ ]]) hiervoor gebruikt kunnen worden. 

```{r}
Betaal_transacties$Bedrag
```
```{r}
Betaal_transacties[["Bedrag"]]
```
```{r}
Betaal_transacties[[1]]
```

Om deze in een pijp te kunnen gebruiken, zul je een speciale referentie moeten gebruiken, de dot (.):

```{r}
Betaal_transacties %>% .$Bedrag
```
```{r}
Betaal_transacties %>% .[["Bedrag"]]
```
```{r}
Betaal_transacties %>% .[[1]]
```

In tegenstelling tot dataframes, staan tibbles geen "partial matching" toe, dwz, je moet de volledige naam correct uitschrijven om naar de kolom(men) te kunnen verwijzen.

__NB__: Het kan voorkomen dat oude functies niet werken met tibbles. Indien je zo'n functie tegenkomt, kun je `as.data.frame()` gebruiken om de tibble terug te converteren naar de klassieke dataframe.

## Tidy Data

## principes van tidy data
Wat is een Tidy dataset? Een tidy dataset is een dataset dat voldoet aan de volgende voorwaarden:

1.  Iedere variabele heeft zijn eigen kolom

1.  Iedere observatie heeft zijn eigen rij

1.  Iedere waarde heeft zijn eigen cell


![](http://r4ds.had.co.nz/images/tidy-1.png)


Waarom is het belangrijk om je data georganiseerd (tidy) te hebben? Twee belangrijke redenen. (1) het is een consistente format voor het opslaan, verwerken en bewerken van je data. Door deze consistentie is het eenvoudiger om nieuwe tools te leren die met de data kunt gebruiken. Daarnaast (2) maakt deze data organisatie het mogelijk om gevectoriseerde operaties uit te voeren, en daarmee de snelheid en kracht van R maximaal te benutten. 

dplyr, ggplot2, en alle andere packages in de tidyverse zijn ontworpen om goed te werken met tidy data. 


### Reshape Functies
Binnen de Tidy library bestaan er twee handige reshape functies: `gather()` & `spread()`. De `gather()` functie manipuleert kolomnamen en hun waarden naar een sleutel-waarde (key-value) constructie. Het gevolg is hierdoor dat je een kleine tabel met vele kolommen kunt ombouwen tot een lange tabel met weinig kolommen.

![](gather.png)

De functie `spread()` doet het omgekeerde: `spread()` herorganiseert de sleutel-waarde (key-value) structuur tot een tabel waar de unieke sleutels (keys) tot kolomnamen worden omgezet en hun bijbehorende waarden "gesmeerd" over de nieuwe kolomnamen.
![](spread.png)


### Splits & Combineer Functies

Soms bevat een geimporteerde data bestand meerdere waarden binnen een kolom. In dat geval is het handig om deze koom te kunnen splitsen naar twee of meerdere kolommen. Dit is mogelijk met de `seperate()` functie. `seperate()` splitst iedere cell in een kolom adhv een seperator (kan ook een reguliere expressie zijn) naar twee of meerdere kolommen.

![](seperate.png)


Andersom, kan het ook voorkomen dat een bepaalde waarde niet in één cel staat, maar verspreid is over twee of meerdere cellen. In dit geval is het handig om deze cellen te kunnen combineren tot één cell, en hier komt `unite()` functie goed van pas.


![](unite.png)


## Data import

R is krachtig genoeg om data in te lezen uit verschillende bestands- en opslagformaten. Hieronder vind je een overzicht van een aantal veelgeruikte inlees functies:


```{r results = 'asis'}
importfuncties = tibble(
  `Base R import functies`=c("read.csv","read.csv2","read.delim", "read.delim2","read.fwf"), 
  `Tidyverse import functies` =c("read_csv","read_csv2","read_delim", "read_delim2","read_fwf"),
  `Functie beschrijving` = c("voor komma gescheiden waarden en komma's als seperator",
                               "voor semikolom gescheiden waarden en komma's als seperator",
                               "voor tab-gescheiden bestanden en met punt als seperator",
                               "voor tab-gescheiden bestanden en met komma als seperator",
                               "voor data met vastgesteld aantal bytes per kolom"))
knitr::kable(importfuncties, caption = "Beschrijving van import functies")

```




Voor iedere Base-R functie in de bovenstaande tabel, geldt dat de argumenten kunnen worden gespecificeerd als volgt:

```{r results = 'asis'}
importfunctiesArg = tibble(
  Argument = c("header","col.names","na.string", "colClasses","stringAsFactors", "sep", "nrows", "encoding"), 
  Beschrijving = c("Bevat de eerste regel de kolomnamen?",
                   "Specifeer hier je karakter vector met kolomnamen",
                   "Welke strings moeten als NA worden behandeld?",
                   "Specificeer hier de karakter vector met de type kollommen. R converteert de kolommen naar de specifieke type",
                   "Indien TRUE (dit is de default), zal R alle deze waarden converteren naar factor vectoren",
                   "Kolom seperator",
                   "Maximaal aantal rijen dat je wilt inlezen",
                   "Specificeer hier je encoding"))
knitr::kable(importfunctiesArg, caption = "Beschrijving van argumenten voor import functies")

```

De tidyverse maken het ook mogelijk om de bovenstaande opties te configureren. Maar toch zijn er een aantal belangrijke verschillen. De `readr` library uit de tidyverse converteerd nooit strings naar factors bij het inlezen van kolommen. 

NB: Zowel base-R als `readr` gebruiken per default standaard de UTF-8 encoding. Dat laatste zal niet altijd mooie resultaten leveren wanneer je data uit oudere systemen inleest die geen UTF-8 als standaard hanteren. Je zult in dat geval de `gues_encoding()` functie moeten gebruiken, en de experimenteren met de `parse_character()` om de juiste encoding te bepalen. Wanneer je eenmaal de encoding hebt gevonden, kun je deze in de __locale__ argument van de importeer functies gebruiken in `readr` functies, en in de encoding argument instellen van de base-R importeer functies.

## Data Export

Net als dat je data importeert om te bewerken en/of analyseren, zul je op den duur je data willen op slaan. Dit kun je doen door het weg te schrijven als bestand op je disk. En ook hier geldt dat iedere beschreven import functie een equivalent bestaat als export functie. Dus ipv `read.csv()`, heb je `write.csv()` voor de Base-R functies. En voor de `read_delim2` functie, heb je de equivalent `write_delim2` enz.


## String Bewerking met StringR


Alle functies in stringr beginnen met str_ en pakken een karakter vector als eerste input argument. Probeer de onderstaande functies uit, en probeer te begrijpen wat er gebeurt.


```{r}
x <- c("why", "video", "cross", "extra", "deal", "authority")
```
```{r}
str_length(x) 
```
```{r}
str_c(x, collapse = ", ")
```
```{r}
str_sub(x, 1, 2)
```

Bijna alle string functies werken met [reguliere expressies]("http://www.regular-expressions.info/tutorial.html"), een beknopte taal voor het beschrijven van patronen in text data. Bijvoorbeeld, de reguliere expressie "[aeiou]" matcht iedere enkelvoudige karakter dat een klinker is:

```{r}
str_subset(x, "[aeiou]")
```
```{r}
str_count(x, "[aeiou]")
```


Er zijn zeven belangrijke string werkwoorden die met patronen werken:

```{r results = 'asis'}
String_functies = tibble(
 Argument = c("str_detect(x,patern)",
              "str_count(x, pattern)",
              "str_subset(x, pattern)", 
              "str_locate(x, pattern)",
              "str_extract(x, pattern)", 
              "str_match(x, pattern)", 
              "str_replace(x, pattern, replacemnt)", 
              "str_split(x, pattern)"),  
   Beschrijving = c("Detecteert of er een match is in de tekst?",
                   "Telt het aantal matches",
                   "Extracteert het matchende onderdeel van de tekst?",
                   "Geeft de locatie van de match",
                   "Extracteert de tekst van de match",
                   "Extracteert delen van de match zoals gedefinieerd in de parameters",
                   "Vervangt de match met de nieuwe tekst",
                   "Splitst de string in meerdere stukken"),
    Voorbeeld = c('str_detect(x, "[aeiou]")',
                  'str_count(x, "[aeiou]")',
                  'str_subset(x, "[aeiou]")',
                  'str_locate(x, "[aeiou]")',
                  'str_extract(x, "[aeiou]")',
                  'str_match(x, "(.)[aeiou](.)")',
                  'str_replace(x, "[aeiou]", "?")',
                  'str_split(c("a,b", "c,d,e"), ",")'))
knitr::kable(String_functies, caption = "Beschrijving van argumenten voor import functies")

```


Zie ook [stringr: modern, consistent string processing]("https://journal.r-project.org/archive/2010-2/RJournal_2010-2_Wickham.pdf") voor meer uitleg en voorbeelden.


## Datum en Tijd Bewerking met Lubridate

Hoe gaan we met datum en tijd data typen om? Hoe creeëren we deze elementen, en hoe kunnen we deze bewerken?

Het beste manier om dit te leren is door ermee te experimenteren. Hieronder is een tabel bijgevoegd met Base-R en de lubridate functies voor het creeëren, bewerken, en berekenen met datum en tijd objecten. We gaan hier straks mee mee experimenteren.


![lubridate](lubridate.png)


## Factor creatie en bewerking met Forcats 

forcats is geen onderdeel van de core packeges van de tidyverse, dus het is nodig om deze expliciet te laden:

```{r}
library(forcats)
```

Factors worden gebruikt om categorische variabelen te beschrijven met een vast en bekende set van waarden. Je kunt factors creeren met de base-R functie `factor()` of de readr functie `readr::parse_factor()`:

```{r}
x1 <- c("Dec", "Apr", "Jan", "Mar")
month_levels <- c( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
```
```{r}
factor(x1, month_levels)
#> [1] Dec Apr Jan Mar
#> Levels: Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
```
```{r}
parse_factor(x1, month_levels)
#> [1] Dec Apr Jan Mar
#> Levels: Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec

```

Het voordeel van de `parse_factor()` functie is dat het een waarschuwing genereert indien de waarden van de input vector x geen geldige waarden zijn van de gedefinieerde levels:

```{r}
x2 <- c("Dec", "Apr", "Jam", "Mar")

```
```{r}
factor(x2, month_levels)
```

```{r}
parse_factor(x2, month_levels)
```

Wanneer je eenmaal een factor variabele hebt, levert forecats helpfuncties om verschillende problemen/issues te kunnen oplossen.

Om de onderstaande functies te demonstreren, zullen we gebruik maken van de `forcats::gss_cat` dataset. Het is subset van de steekproef dataset dat door [General Social Survey]("http://gss.norc.org/") is gewonnen. 


```{r}
gss_cat %>% dplyr::count(partyid)
```

Je kunt de niveau waarden (namen van de levels) van factors wijzigen met `fct_recode()`

```{r}
gss_cat %>%
  mutate(partyid = fct_recode(partyid,
    "Republican, strong"    = "Strong republican",
    "Republican, weak"      = "Not str republican",
    "Independent, near rep" = "Ind,near rep",
    "Independent, near dem" = "Ind,near dem",
    "Democrat, weak"        = "Not str democrat",
    "Democrat, strong"      = "Strong democrat"
  )) %>%
  count(partyid)

```

Merk op dat ongenoemde waarde niveaus (levels) ongetast zijn gelaten, alsook de volgorde van de levels is behouden.

Er zijn verder nog vier handige helpfuncties binnen de forcats package die je voor veel operaties zult gebruiken: 

*  fct_relevel() is gelijksoortig aan `stats::relevel()` maar staat ook toe om genoemde waarden in niveau op te hogen door ze naar voren te halen 

*  fct_inorder() rangschikt volgens de eerste voorkomen van iedere level.

*  fct_infreq() rangschikt van meest voorkomende waaarde naar minst frequente waarde.

*  fct_rev() draait de volgorde van de orde om.


### oefening:

*  Gebruik help om te achterhalen hoe de base-R functie `cut()` werkt.

## Byzondere waarden

Net als de meeste programeer talen, heeft R een aantal speciale waarden die een uitzondering zijn op de normale waarden van een data type. Dit zijn de NA, NULL, ±Inf en NaN waarden. We zullen dit kort toelichten:

*   __NA__ staat voor Not Available is een plaatsvervanger voor een ontbrekende waarde. Alle basis operaties in R kunnen met NA's omgaan zonder te crashen, echter de meeste retourneren een NA als resultaat (waarom?) indien een van de input argumenten ook een NA is. Voorspel het resultaat van de volgende operaties:

```{r}
NA + 1
sum(c(NA, 1, 2))
median(c(NA, 1, 2, 3), na.rm = TRUE)
length(c(NA, 2, 3, 4))
3 == NA
NA == NA
TRUE | NA
```

de functie `is.na()` kan gebruikt worden om NA's te detecteren.

*   __NULL__ is een speciale waarde, aangezien het geen eigen data type is (haar `class()` is NULL) en heeft een lengte van 0, waardoor het geen plek inneemt in een vector. Probeer de resultaten van de volgende operaties te voorspellen: 

```{r}
length(c(1, 2, NULL, 4))
sum(c(1, 2, NULL, 4))
x <- NULL
c(x, 2)

```

De functie `is.null()` kan gebruikt worden om NULL waarden te detecteren.

*   __Inf__ staat voor oneindig (infinity) en kan alleen voorkomen in numerieke vectoren. Technisch gezien, is Inf een geldige numerieke waarden waarop je rekenkundige operaties op kunt uitvoeren. Probeer de resultaten van de volgende operaties te voorspellen:

```{r}
pi/0
2 * Inf
Inf - 1e+10
Inf + Inf
3 < -Inf
Inf == Inf
```


*   __NaN__ Staat voor niet een nummer (Not A Number). Dit is in het algemeen het resultaat van een berekening waarvan het resultaat onbekend is, maar het is absoluut geen nummer. Rekenkundige operaties die leiden tot NaN zijn onder andere: 0/0, Inf-Inf en Inf/Inf. Numerieke operaties met NaN resulteren altijd in een NaN. Nan behoort tot de numerieke data type.

```{r}
NaN + 1
exp(NaN)
```


### Oefeningen:

Voorspel de resultaten van de volgende operaties:


1.   exp(-Inf)

1.   NA == NA

1.   NA == NULL

1.   NULL == NULL

1.   NA & FALSE




# Case: Stack Overflow Dataset

We gaan vanaf nu onze handen vuil maken met de Stackoverflow dataset. Deze is te via de [StackExchange API]("http://api.stackexchange.com/") te verkrijgen, alsook dmv de [Stack Exchange Data Dump]("https://archive.org/details/stackexchange") op de Internet Archive.

## Text files, semi- en ongestructureerde data bestanden

Tot nu toe hebben we import en export functies van gestructureerde bron-bestanden behandeld. SOms hebben we ook te maken met semi-gestructureerde en ongestructureerde data. Hoe lezen we deze data bronnen in? 

__Sla nu al je R bestanden op!!!!__
__Sla nu al je R bestanden op!!!!__
__Sla nu al je R bestanden op!!!!__


### Oefeningen

1.  Hoe lezen we de SONL.JSON data? Vind drie verschillende manieren. Hint: zoek het lekker zelf uit!
1.  Probeer alle drie de methoden uit. Wat werkt wel en wat werkt niet? En waarom werkte het niet?



